# Contributor: Jakub Jirutka <jakub@jirutka.cz>
# Maintainer: Jakub Jirutka <jakub@jirutka.cz>
pkgname=cargo-make
pkgver=0.37.10
pkgrel=0
pkgdesc="Rust task runner and build tool"
url="https://github.com/sagiegurari/cargo-make"
# riscv64: TODO
# s390x: fails to build nix crate
arch="all !riscv64 !s390x"
license="Apache-2.0"
makedepends="cargo openssl-dev cargo-auditable"
subpackages="$pkgname-bash-completion"
source="https://github.com/sagiegurari/cargo-make/archive/$pkgver/cargo-make-$pkgver.tar.gz"
options="!check"  # FIXME: some tests are broken

_cargo_opts="--frozen --no-default-features --features tls-native"


prepare() {
	default_prepare

	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo auditable build $_cargo_opts --release
}

check() {
	cargo test $_cargo_opts
}

package() {
	install -D -m755 -t "$pkgdir"/usr/bin/ \
		target/release/cargo-make \
		target/release/makers

	install -D -m644 extra/shell/makers-completion.bash \
		"$pkgdir"/usr/share/bash-completion/completions/makers
}

sha512sums="
1320fdde4fd9eb5f5aa991023f2ec0d7532fe1ed393622ab57a3c81e80d69dd4726563ca5fd5c9d8ddb45d7e27fef8dfd2da21edbb492fccdffdaa49c6fea11b  cargo-make-0.37.10.tar.gz
"
