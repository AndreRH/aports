# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=py3-speechrecognition
pkgver=3.10.2
pkgrel=0
pkgdesc="Library for performing speech recognition, with support for several engines and APIs, online and offline"
url="https://github.com/Uberi/speech_recognition/"
# s390x blocked by failing tests
arch="noarch !s390x"
license="BSD-3-Clause AND GPL-2.0-only"
depends="
	flac
	py3-pyaudio
	py3-requests
	py3-typing-extensions
	"
makedepends="
	py3-gpep517
	py3-setuptools
	py3-wheel
	"
checkdepends="
	py3-pocketsphinx
	py3-pytest
	"
subpackages="$pkgname-pyc"
source="$pkgname-$pkgver.tar.gz::https://github.com/Uberi/speech_recognition/archive/refs/tags/$pkgver.tar.gz"
builddir="$srcdir/speech_recognition-$pkgver"

prepare() {
	default_prepare

	# Remove pre-built flac binaries, we want to use system ones instead
	rm speech_recognition/flac*
}

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages .testenv
	.testenv/bin/python3 -m installer .dist/*.whl
	.testenv/bin/python3 -m pytest -k 'not test_whisper and not test_google'
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl
}

sha512sums="
163eb211ef2dcb9ec658dae1d1e9b53914362f79066c37e5b0e42cf2465101f246f0830dc667d17bb39047491d04b282abad5a4bba496e6dedb3845dead57744  py3-speechrecognition-3.10.2.tar.gz
"
