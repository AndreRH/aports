# Contributor: Jakub Jirutka <jakub@jirutka.cz>
# Maintainer: Jakub Jirutka <jakub@jirutka.cz>
pkgname=ttyd
pkgver=1.7.6
pkgrel=0
pkgdesc="Share your terminal over the web"
url="https://tsl0922.github.io/ttyd"
arch="all"
license="MIT"
depends="libwebsockets-evlib_uv"
makedepends="
	bsd-compat-headers
	cmake
	json-c-dev
	libuv-dev
	libwebsockets-dev
	openssl-dev>3
	samurai
	zlib-dev
	"
subpackages="$pkgname-doc"
source="https://github.com/tsl0922/ttyd/archive/$pkgver/ttyd-$pkgver.tar.gz
	fix-version.patch
	"

build() {
	local crossopts=
	if [ "$CBUILD" != "$CHOST" ]; then
		crossopts="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_VERBOSE_MAKEFILE=TRUE \
		-DPROJECT_VERSION=$pkgver \
		$crossopts
	cmake --build build
}

check() {
	./build/ttyd --version
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
73220435ecb6cd443866b1902cddcff5667ce4cff3d70fb4259003208700f7cae096815abef2d10f6a022b21ac2bb5c2d14efe2aa6c7c5e73b89b0ff4fab042f  ttyd-1.7.6.tar.gz
ab60cb7e48c8262ef05aec0caf06e97d6400d32f178521195ef7f2ad06be416da0acde0848c2856f2f9bebb09593235f647a88b0e869ddddcc60d48c39329307  fix-version.patch
"
