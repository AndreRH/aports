# Contributor: Guy Godfroy <guy.godfroy@gugod.fr>
# Maintainer: Guy Godfroy <guy.godfroy@gugod.fr>
pkgname=py3-xsdata
pkgver=24.3.1
pkgrel=0
pkgdesc="Naive XML & JSON Bindings for python"
url="https://github.com/tefra/xsdata"
arch="noarch !s390x"
# s390x: because of ruff
#        see https://gitlab.alpinelinux.org/alpine/aports/-/issues/15642
license="MIT"
depends="
	py3-click
	py3-click-default-group
	py3-docformatter
	py3-jinja2
	py3-toposort
	ruff
	"
makedepends="
	py3-gpep517
	py3-setuptools
	py3-wheel
	"
checkdepends="
	py3-lxml
	py3-pytest-benchmark
	py3-pytest-forked
	py3-requests
	"
subpackages="$pkgname-pyc"
source="$pkgname-$pkgver.tar.gz::https://github.com/tefra/xsdata/archive/refs/tags/v$pkgver.tar.gz
	typing.patch
	"
builddir="$srcdir/xsdata-$pkgver"

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages .testenv
	.testenv/bin/python3 -m installer .dist/*.whl
	.testenv/bin/python3 -m pytest --forked
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl
}

sha512sums="
e60e2664df9a3ac08127d5cc8a0d4e59f8357fa3c587331f8cf2faa6a9db25c35a0cd35984006afd4ad579287a8b9ff8c8c935fe8187c48954aad8d66802d7f4  py3-xsdata-24.3.1.tar.gz
3140a89f223a57cafb3c8e331e937188c22abceee1c3492da08e59f7b9928f8fef74a7fa6c348766c41cfe852057beb68639c3c04af09c717569da00f0fb6ea8  typing.patch
"
