# Contributor: Hugo Osvaldo Barrera <hugo@whynothugo.nl>
# Maintainer: Hugo Osvaldo Barrera <hugo@whynothugo.nl>
pkgname=ublock-origin
pkgver=1.57.0
pkgrel=0
pkgdesc="Efficient blocker add-on for Firefox"
url="https://github.com/gorhill/uBlock"
arch="noarch"
license="GPL-3.0-or-later"
makedepends="git python3 zip bash"
# Upstream's build script pulls latest master for uAssets.
# Pin the versions we use so we don't need network at built-time and the package
# sources are deterministic.
_uassets_master=13715d6deb2ab7e384ac79368fd53c9fd8a7ef64
_uassets_ghpages=3da96ce3dd494202656cbc95e1de8baabc0ab7b2
source="ublock-origin-$pkgver.tar.gz::https://github.com/gorhill/uBlock/archive/refs/tags/$pkgver.tar.gz
	uAssets-$_uassets_master.tar.gz::https://github.com/uBlockOrigin/uAssets/archive/$_uassets_master.tar.gz
	uAssets-$_uassets_ghpages.tar.gz::https://github.com/uBlockOrigin/uAssets/archive/$_uassets_ghpages.tar.gz
"
builddir="$srcdir/uBlock-$pkgver"
options="!check" # no tests, build fetches assets

prepare() {
	default_prepare

	mkdir -p dist/build/uAssets
	mv ../uAssets-$_uassets_master dist/build/uAssets/main
	mv ../uAssets-$_uassets_ghpages dist/build/uAssets/prod
}

build() {
	make firefox
}

package() {
	install -Dm644 dist/build/uBlock0.firefox.xpi "$pkgdir/usr/lib/firefox/browser/extensions/uBlock0@raymondhill.net.xpi"
}

sha512sums="
2e5f9e38763e1f646b90eaee8fd9441bc40f42dec1014d44f8bea92718a0cb40d0183d189dd06868897ce6c3d19e194dbe9bd8e83f0d75bf25be009a4684d50b  ublock-origin-1.57.0.tar.gz
6d802242c3609c601a08552234d71501f9b4621cca67e0a4f8ec6b327a505e51252f4ce0f8067750c658a30217ae5b798c50a8cf7afe92ce2b051e5361e05ef1  uAssets-13715d6deb2ab7e384ac79368fd53c9fd8a7ef64.tar.gz
3366412824404bec1d68f04467efd99e080c77c7a93491be6cfad1899ef28a3a7fd33f76b017dd6825d9094f45b7400c3fe2d317696103e352731899c8d4132a  uAssets-3da96ce3dd494202656cbc95e1de8baabc0ab7b2.tar.gz
"
